import { Component, OnInit } from '@angular/core';
import { Hero } from '../classes/hero';
import { HEROES } from '../mocks/mock-heroes';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  hero: Hero = {
    id:0,
    name: "Thor"
  }

  selectedHero:Hero;

  heroes: Array<Hero> = HEROES;
  constructor() { }

  ngOnInit() {
  }

  selectHero(hero:Hero):void{
    this.selectedHero = hero;
  }

}
