import { Pipe, PipeTransform, isDevMode } from '@angular/core';
import { UpperCasePipe } from '@angular/common';

@Pipe({
  name: 'pokemonWrittingPipe'
})
export class PokemonWrittingPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let result:string = '';
    /*
      for...in returns index
      for...of returns object of the collection
    */
    for(let i in value){
      if(this.idOdd(i)){
        result+=value[i];
      }else{
        result+= (value[i] as string).toUpperCase();
      }
    }
    return result;
  }

  idOdd(value:any){
    return value % 2;
  }

}
